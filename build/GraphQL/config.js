"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Resolvers_1 = require("./Resolvers");
const Schema_1 = require("./Schema");
const apollo_server_express_1 = require("apollo-server-express");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const GQservers = new apollo_server_express_1.ApolloServer({
    typeDefs: Schema_1.typeDefs,
    resolvers: Resolvers_1.resolvers,
    playground: true,
    introspection: true,
    context: ({ req }) => __awaiter(void 0, void 0, void 0, function* () {
        const token = req.headers["authorization"];
        if (token !== null) {
            try {
                const usuarioActual = yield jsonwebtoken_1.default.verify(token, process.env.SECRETO);
                req.usuarioActual = usuarioActual;
                return {
                    usuarioActual,
                };
            }
            catch (err) {
                //console.log('err: ', err);
            }
        }
    }),
});
exports.default = GQservers;
