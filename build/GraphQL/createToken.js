"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.crearToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const crearToken = (user, secreto, expiresIn) => {
    const { _id } = user;
    return jsonwebtoken_1.default.sign({ _id }, secreto, { expiresIn });
};
exports.crearToken = crearToken;
