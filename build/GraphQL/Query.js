"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Query = void 0;
const user_1 = __importDefault(require("../models/user"));
const comunicado_1 = __importDefault(require("../models/comunicado"));
const validateEmail_1 = require("./validateEmail");
const post_1 = __importDefault(require("../models/post"));
const ads_1 = __importDefault(require("../models/ads"));
const proveedor_1 = __importDefault(require("../models/proveedor"));
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
exports.Query = {
    getUsuario: (root, {}, { usuarioActual }) => {
        if (!usuarioActual) {
            return {
                success: false,
                message: "Debes iniciar sesión para continuar",
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: usuarioActual._id }, (err, res) => {
                    if (err) {
                        rejects({
                            message: "Algo salio mal intentalo de nuevo",
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            message: "Bienbenido a la Federación de Hostelería",
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getComunicados: (root, { type }, {}) => {
        return new Promise((resolve, rejects) => {
            comunicado_1.default
                .find({ type: type }, (err, res) => {
                if (err) {
                    rejects({
                        message: "Algo salio mal intentalo de nuevo",
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        message: "Todo a salido bien",
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getUserAdmin: (root, { search, page, limit }) => __awaiter(void 0, void 0, void 0, function* () {
        var value = yield user_1.default.countDocuments();
        return new Promise((resolve, reject) => {
            let condition = {};
            //@ts-ignore
            if (search)
                if (validateEmail_1.validateEmail(search)) {
                    //@ts-ignore
                    condition.email = search;
                }
                else {
                    condition = {
                        $text: { $search: `"\"${search} \""` },
                    };
                }
            user_1.default
                .find(condition, (error, user) => {
                if (error) {
                    reject({
                        success: false,
                        message: "There is a problem with your request",
                        status: 404,
                        count: 0,
                        data: null,
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Successful operation",
                        status: 200,
                        count: value,
                        data: user,
                    });
                }
            })
                .sort(search ? null : { $natural: -1 })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    }),
    getPost: (root, {}, {}) => {
        return new Promise((resolve, rejects) => {
            post_1.default
                .find((err, res) => {
                if (err) {
                    rejects({
                        message: "Algo salio mal intentalo de nuevo",
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        message: "Todo a salido bien",
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getComunicadosID: (root, { id }, {}) => {
        if (id) {
            return new Promise((resolve, rejects) => {
                comunicado_1.default.findOne({ _id: ObjectId(id) }, (err, res) => {
                    console.log("resd", res, ObjectId(id));
                    if (err) {
                        rejects({
                            message: "Algo salio mal intentalo de nuevo",
                            success: false,
                            data: null,
                        });
                    }
                    else {
                        resolve({
                            message: "Todo a salido bien",
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getPostParam: (root, { input, page, limit }, {}) => {
        return new Promise((resolve, rejects) => {
            post_1.default
                .find(input, (err, res) => {
                if (err) {
                    rejects({
                        message: "Algo salio mal intentalo de nuevo",
                        success: false,
                        data: null,
                    });
                }
                else {
                    resolve({
                        message: "Todo a salido bien",
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
    getAds: (root, {}) => {
        let matchQuery1 = {};
        return new Promise((resolve, rejects) => {
            ads_1.default.aggregate([{ $match: matchQuery1 }, { $sample: { size: 150 } }], (err, res) => {
                if (err) {
                    rejects({
                        messages: "Sisten Error",
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: "Ads success",
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getProveedor: (root, { search, page, limit }) => __awaiter(void 0, void 0, void 0, function* () {
        var value = yield proveedor_1.default.countDocuments();
        return new Promise((resolve, reject) => {
            let condition = {};
            //@ts-ignore
            if (search)
                condition = {
                    //@ts-ignore
                    $text: { $search: `"\"${search} \""` },
                };
            proveedor_1.default
                .find(condition, (error, user) => {
                if (error) {
                    reject({
                        success: false,
                        message: "There is a problem with your request",
                        status: 404,
                        count: 0,
                        data: null,
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Successful operation",
                        status: 200,
                        count: value,
                        data: user,
                    });
                }
            })
                .sort(search ? null : { $natural: -1 })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    }),
};
