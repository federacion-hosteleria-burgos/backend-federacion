"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mutation = void 0;
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const createToken_1 = require("./createToken");
// Models
const user_1 = __importDefault(require("../models/user"));
const comunicado_1 = __importDefault(require("../models/comunicado"));
const post_1 = __importDefault(require("../models/post"));
const ads_1 = __importDefault(require("../models/ads"));
const proveedor_1 = __importDefault(require("../models/proveedor"));
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
aws_sdk_1.default.config.update({
    region: process.env.region,
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey,
});
var s3BucketStore = new aws_sdk_1.default.S3({ params: { Bucket: "header-store-wilbby" } });
exports.Mutation = {
    LoginUser: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield user_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: "Aún no de te has registrado en la Federación",
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (users.isAvalible) {
            if (!passwordCorrecto) {
                return {
                    success: false,
                    message: "Contraseña incorrecta",
                    data: null,
                };
            }
            else {
                return {
                    success: true,
                    message: "Bienvenido a la Federación de Hostelería de Burgos",
                    data: {
                        token: createToken_1.crearToken(users, process.env.SECRETO, "9999 years"),
                        id: users._id,
                        user: users,
                        verifyPhone: users.verifyPhone,
                    },
                };
            }
        }
        else {
            return {
                success: false,
                message: "Tu cuenta ha sido desactivada",
                data: null,
            };
        }
    }),
    crearUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const emailExists = yield user_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Algo salio mal intentalo de nuevo",
                data: null,
            };
        }
        const nuevoUsuario = new user_1.default(input);
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => __awaiter(void 0, void 0, void 0, function* () {
                console.log(error);
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Bienvenido a la Federación de Hostelería de Burgos",
                        data: nuevoUsuario,
                    });
                }
            }));
        });
    }),
    actualizarUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Datos acualizado con exito",
                        data: _usuario,
                    });
                }
            });
        });
    }),
    eliminarUsuario: (root, { id }) => {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndDelete({ _id: id }, 
            //@ts-ignore
            (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Comunicado eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    crearCommunicado: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevoComunicado = new comunicado_1.default(input);
        return new Promise((resolve, object) => {
            nuevoComunicado.save((error) => __awaiter(void 0, void 0, void 0, function* () {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Entrada creada con éxito",
                        data: nuevoComunicado,
                    });
                }
            }));
        });
    }),
    eliminarComunication: (root, { id }) => {
        return new Promise((resolve, reject) => {
            comunicado_1.default.findOneAndDelete({ _id: id }, 
            //@ts-ignore
            (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Comunicado eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    crearPost: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevoPost = new post_1.default(input);
        return new Promise((resolve, object) => {
            nuevoPost.save((error) => __awaiter(void 0, void 0, void 0, function* () {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Entrada creada con éxito",
                        data: nuevoPost,
                    });
                }
            }));
        });
    }),
    eliminarPost: (root, { id }) => {
        return new Promise((resolve, reject) => {
            post_1.default.findOneAndDelete({ _id: id }, 
            //@ts-ignore
            (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Entrada eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    createAds: (root, { data }) => {
        const nuevaAds = new ads_1.default(data.data);
        return new Promise((resolve, rejects) => {
            nuevaAds.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: "Anuncio creado con exito",
                        success: true,
                    });
                }
            });
        });
    },
    updateAds: (root, { data }) => {
        return new Promise((resolve, rejects) => {
            ads_1.default.findOneAndUpdate({ _id: data._id }, data, { new: true }, (error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: "Anuncio acualizado con exito",
                        success: true,
                    });
                }
            });
        });
    },
    eliminarAds: (root, { id }) => {
        return new Promise((resolve, reject) => {
            ads_1.default.findOneAndDelete({ _id: id }, 
            //@ts-ignore
            (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Producto eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    singleUploadToStoreImagenAws(parent, { file }) {
        const buf = Buffer.from(file.replace(/^data:image\/\w+;base64,/, ""), "base64");
        var data = {
            Key: `${Date.now()}-federacion-burgos`,
            Body: buf,
            ContentEncoding: "base64",
            ContentType: "image/jpeg",
            ACL: "public-read",
        };
        return new Promise((resolve, reject) => {
            //@ts-ignore
            s3BucketStore.upload(data, function (err, data) {
                if (err) {
                    reject({
                        data: "Imagen muy grandel reduce el tamaño por favor",
                    });
                }
                else {
                    resolve({
                        data: data,
                    });
                }
            });
        });
    },
    createProveedor: (root, { input }) => {
        const nuevaproveedorSchema = new proveedor_1.default(input);
        return new Promise((resolve, rejects) => {
            nuevaproveedorSchema.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: "Proveedor creado con exito",
                        success: true,
                    });
                }
            });
        });
    },
    updateProveedor: (root, { input }) => {
        return new Promise((resolve, rejects) => {
            proveedor_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: "Anuncio acualizado con exito",
                        success: true,
                    });
                }
            });
        });
    },
    eliminarProveedor: (root, { id }) => {
        return new Promise((resolve, reject) => {
            proveedor_1.default.findOneAndDelete({ _id: id }, 
            //@ts-ignore
            (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Producto eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
};
