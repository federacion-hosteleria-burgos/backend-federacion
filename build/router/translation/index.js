"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const { Translate } = require("@google-cloud/translate").v2;
const CREDENTIAL = JSON.parse(process.env.CREDENTIALS);
const options = {
    credentials: CREDENTIAL,
    projectId: CREDENTIAL.project_id,
};
const translate = new Translate(options);
const routerTranslation = express_1.Router();
routerTranslation.post("/translate-text", (req, res) => {
    //@ts-ignore
    const data = req.body;
    const text = data.text;
    const originLanguage = data.target;
    function translateText() {
        return __awaiter(this, void 0, void 0, function* () {
            let [detections] = yield translate.detect(text);
            const target = originLanguage === detections.language
                ? originLanguage === "es" && detections.language === "es"
                    ? "en"
                    : "es"
                : originLanguage;
            let [translations] = yield translate.translate(text, target);
            translations = Array.isArray(translations) ? translations : [translations];
            translations.forEach((translation, i) => {
                res
                    .status(200)
                    .json({
                    text: translation,
                    target: target,
                    detections: detections.language,
                    success: true,
                })
                    .end();
            });
        });
    }
    translateText();
});
exports.default = routerTranslation;
