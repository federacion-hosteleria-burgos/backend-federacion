"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ContactEmail_1 = __importDefault(require("./ContactEmail"));
const AddUserEmail_1 = __importDefault(require("./AddUserEmail"));
const mailjetTransport = require('nodemailer-mailjet-transport');
const nodemailer = require("nodemailer");
const router = express_1.Router();
let transporter = nodemailer.createTransport(mailjetTransport({
    service: 'Mailjet',
    host: "in-v3.mailjet.com",
    secure: true,
    port: 587,
    auth: {
        apiKey: "377b611f6f09116fd2097247933ea2b7",
        apiSecret: "d40570597e56453d397cd282e218e57e"
    }
}));
router.post("/contact", (req, res) => {
    const mailOptions = {
        from: "info@federacionhosteleriaburgos.es",
        to: `federacionhosteleriaburgos@gmail.com`,
        subject: "FORMULARIO DE CONTACTO DE LA WEB",
        text: "FORMULARIO DE CONTACTO DE LA WEB",
        html: ContactEmail_1.default(req.body),
    };
    transporter.sendMail(mailOptions);
    res.json({
        success: true,
        messages: "Datos enviados con éxito en 24 o 48 horas te contactaremos",
    });
});
router.post("/add-member", (req, res) => {
    const mailOptions = {
        from: "info@federacionhosteleriaburgos.es",
        to: `federacionhosteleriaburgos@gmail.com`,
        subject: "SOLICITUD PARA UNIRSE A LA FEDERACIÓN",
        text: "SOLICITUD PARA UNIRSE A LA FEDERACIÓN",
        html: AddUserEmail_1.default(req.body),
    };
    transporter.sendMail(mailOptions);
    res.json({
        success: true,
        messages: "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
    });
});
exports.default = router;
