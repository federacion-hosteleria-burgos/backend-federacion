"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const crypto_1 = __importDefault(require("crypto"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const RecoveryPassword_1 = __importDefault(require("./Templates/RecoveryPassword"));
const mailjetTransport = require("nodemailer-mailjet-transport");
let transporter = nodemailer_1.default.createTransport(mailjetTransport({
    service: "Mailjet",
    host: "in-v3.mailjet.com",
    secure: true,
    port: 587,
    auth: {
        apiKey: "377b611f6f09116fd2097247933ea2b7",
        apiSecret: "d40570597e56453d397cd282e218e57e",
    },
}));
const Recoverrouter = express_1.Router();
Recoverrouter.get("/forgotpassword", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const email = req.query.email;
    yield user_1.default.find({ email: email }, (err, data) => {
        if (data.length < 1 || err) {
            res.status(400).json({
                message: "Aún no tenemos este correo eletrónico",
                success: false,
                noEmail: true,
            });
        }
        else {
            const token = crypto_1.default.randomBytes(20).toString("hex");
            const mailOptions = {
                from: "info@federacionhosteleriaburgos.es",
                to: email,
                subject: "Recuperar o crear contraseña de la Federación",
                text: "Recuperar o crear Recuperar contraseña de la Federación",
                html: RecoveryPassword_1.default(token, email),
            };
            transporter.sendMail(mailOptions);
            var newvalues = { $set: { forgotPasswordToken: token } };
            user_1.default.findOneAndUpdate({ email: email }, newvalues, {
                //options
                new: true,
                strict: false,
                useFindAndModify: false,
            }, (err, updated) => {
                res.status(200).json({ message: "Email enviado", success: true });
            });
        }
    });
}));
Recoverrouter.post("/tokenValidation", (req, res) => {
    user_1.default.find({ forgotPasswordToken: req.body.token }, (err, data) => {
        if (err || data.length < 1) {
            if (err)
                console.log(err);
            res.status(200).json({ isValid: false, email: "" });
        }
        else if (data.length > 0) {
            res.status(200).json({ isValid: true, email: data[0].email });
        }
    });
});
Recoverrouter.post("/resetPassword", (req, res) => {
    var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
    user_1.default.findOneAndUpdate({ email: req.body.email }, newvalues, {
        //options
        new: true,
        strict: false,
        useFindAndModify: false,
    }, (err, updated) => {
        if (err)
            console.log(err);
        bcryptjs_1.default.genSalt(10, (err, salt) => {
            if (err)
                console.log(err);
            bcryptjs_1.default.hash(req.body.password, salt, (err, hash) => {
                if (err)
                    console.log(err);
                var newvalues2 = { $set: { password: hash } };
                user_1.default.findOneAndUpdate({ email: req.body.email }, newvalues2, {
                    //options
                    new: true,
                    strict: false,
                    useFindAndModify: false,
                }, (err, updated) => {
                    if (err)
                        console.log(err);
                    res.status(200).json({ changed: true });
                });
            });
        });
    });
});
exports.default = Recoverrouter;
