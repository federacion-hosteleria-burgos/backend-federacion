"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const index_router_1 = __importDefault(require("./router/index.router"));
const Login_1 = __importDefault(require("./Login"));
const helmet_1 = __importDefault(require("helmet"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const ConfirmNumber_1 = __importDefault(require("./Twilio/ConfirmNumber"));
const recoverPassword_1 = __importDefault(require("./Emails/recoverPassword"));
const translation_1 = __importDefault(require("./router/translation"));
const app = express_1.default();
app.use(cors_1.default());
app.use(morgan_1.default("dev"));
app.use(helmet_1.default({
    contentSecurityPolicy: process.env.NODE_ENV === "production" ? undefined : false,
}));
app.use(compression_1.default());
app.use(body_parser_1.default.urlencoded({
    parameterLimit: 100000,
    limit: "50mb",
    extended: true,
}));
app.use(body_parser_1.default.json({ limit: "50mb", type: "application/json" }));
app.use(function (req, res, next) {
    //@ts-ignore
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Origin", req.headers.origin);
    res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN");
    next();
});
app.use(index_router_1.default);
app.use(Login_1.default);
app.use(ConfirmNumber_1.default);
app.use(recoverPassword_1.default);
app.use(translation_1.default);
exports.default = app;
