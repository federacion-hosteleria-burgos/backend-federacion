"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const routerLogin = express_1.Router();
routerLogin.post("/api/v1/auth/social/mobile", function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        require("mongoose").model("user").schema.add({
            appleToken: String,
            socialNetworks: String,
            isSocial: Boolean,
        });
        let data = req.body;
        const tokens = data.token;
        let emailExistsApple = yield user_1.default.findOne({
            appleToken: data.token,
        });
        if (emailExistsApple) {
            const nuevoUsuario = emailExistsApple;
            res.json({ nuevoUsuario, token: tokens });
        }
        else {
            let emailExists = yield user_1.default.findOne({
                email: data.email,
            });
            if (emailExists) {
                bcryptjs_1.default.genSalt(10, (err, salt) => {
                    if (err)
                        console.log(err);
                    bcryptjs_1.default.hash(data.token, salt, (err, hash) => {
                        if (err)
                            console.log(err);
                        user_1.default.findOneAndUpdate({ email: data.email }, { password: hash }, 
                        //@ts-ignore
                        (err, user) => {
                            if (err) {
                                res.json(err);
                            }
                            let nuevoUsuario = user;
                            res.json({ nuevoUsuario, token: tokens });
                        });
                    });
                });
            }
            else {
                const nuevoUsuarios = new user_1.default({
                    name: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    password: data.token,
                    isSocial: true,
                    city: data.city,
                    appleToken: data.token,
                    socialNetworks: data.provide,
                });
                nuevoUsuarios.id = nuevoUsuarios._id;
                nuevoUsuarios.save((error) => __awaiter(this, void 0, void 0, function* () {
                    if (error) {
                        return res.json(error);
                    }
                    else {
                        /* await stripe.customers.create(
                                {
                                  name: data.firstName,
                                  email: data.email,
                                  description: "Clientes de Wilbby",
                                },
                                function (err: any, customer: any) {
                                  userSchema.findOneAndUpdate(
                                    { _id: nuevoUsuarios._id },
                                    {
                                      $set: {
                                        StripeID: customer.id,
                                      },
                                    },
                                    (err, customers) => {
                                      if (err) {
                                        console.log(err);
                                      }
                                    }
                                  );
                                }
                              ); */
                        const nuevoUsuario = nuevoUsuarios;
                        return res.json({ nuevoUsuario, token: tokens });
                    }
                }));
            }
        }
    });
});
exports.default = routerLogin;
