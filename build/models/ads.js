"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const adsSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        text: true,
        required: true,
    },
    image: {
        type: String,
        required: true,
    },
    sorting: { type: Number },
    includeCity: { type: [String] },
    visible: { type: Boolean },
    navigate: { type: Boolean },
    url: { type: String },
    email: { type: String },
    category: { type: String },
    isHome: { type: Boolean },
    dataEmail: { type: mongoose_1.default.Schema.Types.Mixed },
    click: { type: Number, default: 1 },
    end_date: { type: Date, default: new Date() },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("ads", adsSchema);
