"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const postSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
    },
    image: {
        type: String,
        required: true,
        text: true,
    },
    description: {
        type: String,
        required: true,
    },
    link: {
        type: String,
        required: true,
    },
    type: {
        type: Boolean,
        required: true,
    },
    event: {
        type: Boolean,
        required: true,
    },
    revista: {
        type: Boolean,
        required: true,
    },
    user: {
        type: mongoose_1.default.Schema.Types.Mixed,
        required: true,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("post", postSchema);
