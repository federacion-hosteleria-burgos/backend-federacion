"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const proveedorSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        text: true,
    },
    imagen: {
        type: String,
        required: true,
        text: true,
    },
    description: {
        type: String,
        required: true,
        text: true,
    },
    mark: {
        type: String,
        required: true,
        text: true,
    },
    products: {
        type: [String],
        required: true,
        text: true,
    },
    phone: {
        type: String,
        required: true,
    },
    city: { type: String },
    cede: { type: String },
    vendedores: {
        type: [mongoose_1.default.Schema.Types.Mixed],
        required: true,
    },
    url: { type: String },
    user: { type: String },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("proveedor", proveedorSchema);
