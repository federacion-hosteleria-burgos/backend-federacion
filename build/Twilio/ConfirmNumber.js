"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const routerPhone = express_1.Router();
const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const SERVICESID = process.env.SERVICESID;
const client = require("twilio")(accountSid, authToken, SERVICESID);
routerPhone.post("/verify-phone", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { phone } = req.body;
    const channel = "sms";
    yield client.verify
        .services(SERVICESID)
        .verifications.create({ to: `+${phone}`, channel })
        .then((message) => {
        res.status(200).json({ success: true, data: message }).end();
    })
        .catch((err) => {
        console.log("Err sen message", err);
        res.status(404).json({ success: false, data: err }).end();
    });
}));
routerPhone.post("/verify-code", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { phone, code, id } = req.body;
    client.verify
        .services(SERVICESID)
        .verificationChecks.create({ to: `+${phone}`, code: code })
        .then((verification_check) => {
        if (verification_check.status === "approved") {
            user_1.default.findOneAndUpdate({ _id: id }, {
                $set: { verifyPhone: true, phone: phone },
            }, 
            //@ts-ignore
            (err, user) => {
                if (err) {
                    res.status(200).json({ success: false, data: err }).end();
                }
                else {
                    res
                        .status(200)
                        .json({ success: true, data: verification_check })
                        .end();
                }
            });
        }
        else {
            res
                .status(200)
                .json({ success: false, data: verification_check })
                .end();
        }
    })
        .catch((err) => {
        res.status(200).json({ success: false, data: err }).end();
    });
}));
routerPhone.post("/send-sms-to-client", (req, res) => {
    const { telefono, mensageMovil } = req.body;
    client.messages
        .create({
        body: mensageMovil,
        from: "RENTYT",
        to: `+${telefono}`,
    })
        .then((message) => res.json({ data: "Mensaje enviado con éxito", success: true }).end())
        .catch((err) => res.json({ data: "Algo va mal intentalo de nuevo", success: false }).end());
});
exports.default = routerPhone;
