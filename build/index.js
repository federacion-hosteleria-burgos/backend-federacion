"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const http_1 = __importDefault(require("http"));
require("./db/connect");
const config_1 = __importDefault(require("./GraphQL/config"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const server = http_1.default.createServer(app_1.default);
app_1.default.set("port", process.env.PORT || 4400);
//@ts-ignore
config_1.default.applyMiddleware({ app: app_1.default });
server.listen(app_1.default.get("port"), () => {
    console.log("server on port", `localhost:${app_1.default.get("port")}${config_1.default.graphqlPath}`);
});
