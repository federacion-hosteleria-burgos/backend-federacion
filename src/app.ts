import express, { Request, Response, NextFunction } from "express";
import morgan from "morgan";
import mainRouter from "./router/index.router";
import loginRouter from "./Login";
import helmet from "helmet";
import compression from "compression";
import cors from "cors";
import bodyParser from "body-parser";
import phoneRouter from "./Twilio/ConfirmNumber";
import emailRouter from "./Emails/recoverPassword";
import routerTranslation from "./router/translation";

const app = express();

app.use(cors());
app.use(morgan("dev"));
app.use(
  helmet({
    contentSecurityPolicy:
      process.env.NODE_ENV === "production" ? undefined : false,
  })
);
app.use(compression());
app.use(
  bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: "50mb",
    extended: true,
  })
);
app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));

app.use(function (req: Request, res: Response, next: NextFunction) {
  //@ts-ignore
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN"
  );
  next();
});

app.use(mainRouter);
app.use(loginRouter);
app.use(phoneRouter);
app.use(emailRouter);
app.use(routerTranslation);

export default app;
