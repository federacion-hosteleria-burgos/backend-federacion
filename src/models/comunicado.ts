import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const comunicadoSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      text: true,
    },
    type: {
      type: String,
      required: true,
      text: true,
    },

    content: {
      type: String,
      required: true,
    },

    user: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IComunicado extends Document {
  title: string;
  type: string;
  content: string;
  created_at: Date;
  user: any;
}

export default mongoose.model<IComunicado>("comunicado", comunicadoSchema);
