import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const proveedorSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      text: true,
    },
    imagen: {
      type: String,
      required: true,
      text: true,
    },

    description: {
      type: String,
      required: true,
      text: true,
    },

    mark: {
      type: String,
      required: true,
      text: true,
    },

    products: {
      type: [String],
      required: true,
      text: true,
    },

    phone: {
      type: String,
      required: true,
    },

    city: { type: String },

    cede: { type: String },

    vendedores: {
      type: [mongoose.Schema.Types.Mixed],
      required: true,
    },

    url: { type: String },

    user: { type: String },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IProveedor extends Document {
  name: string;
  imagen: string;
  description: string;
  mark: string;
  products: string[];
  phone: string;
  city: string;
  cede: string;
  vendedores: any[];
  user: string;
  created_at: Date;
}

export default mongoose.model<IProveedor>("proveedor", proveedorSchema);
