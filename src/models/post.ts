import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      text: true,
    },
    image: {
      type: String,
      required: true,
      text: true,
    },

    description: {
      type: String,
      required: true,
    },

    link: {
      type: String,
      required: true,
    },

    type: {
      type: Boolean,
      required: true,
    },

    event: {
      type: Boolean,
      required: true,
    },

    revista: {
      type: Boolean,
      required: true,
    },

    user: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IPost extends Document {
  title: string;
  type: boolean;
  image: string;
  description: string;
  created_at: Date;
  link: string;
  user: any;
  event: boolean;
  revista: boolean;
}

export default mongoose.model<IPost>("post", postSchema);
