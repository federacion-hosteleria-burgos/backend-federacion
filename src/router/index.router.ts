import { Router, Request, Response } from "express";
import ContactEmail from "./ContactEmail";
import AdduserEmail from "./AddUserEmail";
const mailjetTransport = require('nodemailer-mailjet-transport');
const nodemailer = require("nodemailer");

const router = Router();

let transporter = nodemailer.createTransport(mailjetTransport({
  service: 'Mailjet',
  host: "in-v3.mailjet.com",
  secure: true,
  port: 587,
  auth: {
    apiKey: "377b611f6f09116fd2097247933ea2b7",
    apiSecret: "d40570597e56453d397cd282e218e57e"
  }
}));



router.post("/contact", (req: any, res: Response) => {
 
  const mailOptions = {
    from: "info@federacionhosteleriaburgos.es",
    to: `federacionhosteleriaburgos@gmail.com`,
    subject: "FORMULARIO DE CONTACTO DE LA WEB",
    text: "FORMULARIO DE CONTACTO DE LA WEB",
    html: ContactEmail(req.body),
  };
  transporter.sendMail(mailOptions);

  res.json({
    success: true,
    messages: "Datos enviados con éxito en 24 o 48 horas te contactaremos",
  });
});

router.post("/add-member", (req: any, res: Response) => {
  const mailOptions = {
    from: "info@federacionhosteleriaburgos.es",
    to: `federacionhosteleriaburgos@gmail.com`,
    subject: "SOLICITUD PARA UNIRSE A LA FEDERACIÓN",
    text: "SOLICITUD PARA UNIRSE A LA FEDERACIÓN",
    html: AdduserEmail(req.body),
  };
  transporter.sendMail(mailOptions);
  res.json({
    success: true,
    messages:
      "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
  });
});

export default router;
