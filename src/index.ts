import app from "./app";
import http from "http";
import "./db/connect";
import GQservers from "./GraphQL/config";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

const server = http.createServer(app);

app.set("port", process.env.PORT || 4400);

//@ts-ignore
GQservers.applyMiddleware({ app });

server.listen(app.get("port"), () => {
  console.log(
    "server on port",
    `localhost:${app.get("port")}${GQservers.graphqlPath}`
  );
});
