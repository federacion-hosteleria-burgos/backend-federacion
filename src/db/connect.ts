import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

mongoose.connect(process.env.MONGODBD_URL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,

}).then(() => {
  console.log("db is connect succefully")
}).catch(error => console.log(error));