import { gql } from "apollo-server-express";

export const typeDefs = gql`
  scalar Date
  scalar JSON
  scalar JSONObject

  type generalResponse {
    messages: String
    success: Boolean
  }

  type OauthUserResponse {
    success: Boolean!
    message: String
    data: OauthUser
  }

  type OauthUser {
    token: String!
    id: String!
  }

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type ComunicadoResponse {
    success: Boolean!
    message: String
    data: Comunicado
  }

  type ComunicadoResponseAll {
    success: Boolean!
    message: String
    data: [Comunicado]
  }

  type userResponseAdmin {
    success: Boolean!
    message: String
    status: Int
    count: Int
    data: [Usuario]
  }

  type PostResponse {
    success: Boolean!
    message: String
    data: Post
  }

  type PostResponseAll {
    success: Boolean!
    message: String
    data: [Post]
  }

  type AdsRondomResponse {
    messages: String!
    success: Boolean!
    data: [Ads]
  }

  type Post {
    _id: ID
    title: String
    type: Boolean
    event: Boolean
    revista: Boolean
    image: String
    description: String
    created_at: Date
    link: String
    user: JSON
  }

  type Usuario {
    _id: String
    name: String
    lastName: String
    email: String
    city: String
    avatar: String
    phone: String
    created_at: Date
    updated_at: Date
    termAndConditions: Boolean
    isAvalible: Boolean
    nie: String
    adress: JSON
    isAdmin: Boolean
    createdBy: String
    iban: String
  }

  type Comunicado {
    _id: String
    title: String
    type: String
    content: String
    created_at: Date
    user: JSON
  }

  type Ads {
    _id: ID
    name: String
    image: String
    sorting: Int
    visible: Boolean
    navigate: Boolean
    url: String
    email: String
    click: Int
    category: String
    isHome: Boolean
    dataEmail: JSON
    includeCity: [String]
    created_at: Date
    end_date: Date
  }

  type proveedorResponse {
    success: Boolean
    message: String
    status: Int
    count: Int
    data: [Proveedor]
  }

  type Proveedor {
    _id: ID
    name: String
    imagen: String
    description: String
    mark: String
    products: [String]
    phone: String
    city: String
    cede: String
    vendedores: JSON
    user: String
    created_at: Date
    url: String
  }

  type FileAWS {
    data: JSON
  }

  input AdsData {
    data: JSON
  }

  type Query {
    getUsuario: CrearUsuarioResponse
    getComunicados(type: String): ComunicadoResponseAll
    getUserAdmin(search: String, page: Int, limit: Int): userResponseAdmin
    getPost: PostResponseAll
    getComunicadosID(id: ID): ComunicadoResponse
    getPostParam(input: JSON, page: Int, limit: Int): PostResponseAll
    getAds: AdsRondomResponse
    getProveedor(search: String, page: Int, limit: Int): proveedorResponse
  }

  type Mutation {
    LoginUser(email: String, password: String): OauthUserResponse
    crearUsuario(input: JSON): CrearUsuarioResponse
    actualizarUsuario(input: JSON): CrearUsuarioResponse
    crearCommunicado(input: JSON): ComunicadoResponse
    eliminarComunication(id: ID): generalResponse
    eliminarUsuario(id: ID): generalResponse
    crearPost(input: JSON): PostResponse
    eliminarPost(id: ID): generalResponse
    createAds(data: AdsData): generalResponse
    updateAds(data: JSON): generalResponse
    eliminarAds(id: String): generalResponse
    singleUploadToStoreImagenAws(file: Upload): FileAWS
    createProveedor(input: JSON): generalResponse
    updateProveedor(input: JSON): generalResponse
    eliminarProveedor(id: ID): generalResponse
  }
`;
