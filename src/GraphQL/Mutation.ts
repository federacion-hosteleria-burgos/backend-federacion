import bcrypt from "bcryptjs";
import { crearToken } from "./createToken";
// Models
import userSchema from "../models/user";
import comunicadoSchema from "../models/comunicado";
import postSchema from "../models/post";
import adsSchema from "../models/ads";
import proveedorSchema from "../models/proveedor";
import AWS from "aws-sdk";
import dotenv from "dotenv";

dotenv.config({ path: "variables.env" });

AWS.config.update({
  region: process.env.region,
  accessKeyId: process.env.accessKeyId,
  secretAccessKey: process.env.secretAccessKey,
});

var s3BucketStore = new AWS.S3({ params: { Bucket: "header-store-wilbby" } });

export const Mutation = {
  LoginUser: async (root: any, { email, password }) => {
    const users = await userSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: "Aún no de te has registrado en la Federación",
        data: null,
      };
    }

    const passwordCorrecto = await bcrypt.compare(password, users.password);

    if (users.isAvalible) {
      if (!passwordCorrecto) {
        return {
          success: false,
          message: "Contraseña incorrecta",
          data: null,
        };
      } else {
        return {
          success: true,
          message: "Bienvenido a la Federación de Hostelería de Burgos",
          data: {
            token: crearToken(users, process.env.SECRETO, "9999 years"),
            id: users._id,
            user: users,
            verifyPhone: users.verifyPhone,
          },
        };
      }
    } else {
      return {
        success: false,
        message: "Tu cuenta ha sido desactivada",
        data: null,
      };
    }
  },

  crearUsuario: async (root: any, { input }) => {
    const emailExists = await userSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Algo salio mal intentalo de nuevo",
        data: null,
      };
    }

    const nuevoUsuario = new userSchema(input);

    return new Promise((resolve, object) => {
      nuevoUsuario.save(async (error) => {
        console.log(error);
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Bienvenido a la Federación de Hostelería de Burgos",
            data: nuevoUsuario,
          });
        }
      });
    });
  },

  actualizarUsuario: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) {
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: null,
            });
          } else {
            resolve({
              success: true,
              message: "Datos acualizado con exito",
              data: _usuario,
            });
          }
        }
      );
    });
  },

  eliminarUsuario: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndDelete(
        { _id: id },
        //@ts-ignore
        (error: any) => {
          if (error)
            reject({
              messages: "Hubo un problema con su solicitud",
              success: false,
            });
          else {
            resolve({
              messages: "Comunicado eliminado con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  crearCommunicado: async (root: any, { input }) => {
    const nuevoComunicado = new comunicadoSchema(input);
    return new Promise((resolve, object) => {
      nuevoComunicado.save(async (error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Entrada creada con éxito",
            data: nuevoComunicado,
          });
        }
      });
    });
  },

  eliminarComunication: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      comunicadoSchema.findOneAndDelete(
        { _id: id },
        //@ts-ignore
        (error: any) => {
          if (error)
            reject({
              messages: "Hubo un problema con su solicitud",
              success: false,
            });
          else {
            resolve({
              messages: "Comunicado eliminado con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  crearPost: async (root: any, { input }) => {
    const nuevoPost = new postSchema(input);
    return new Promise((resolve, object) => {
      nuevoPost.save(async (error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Entrada creada con éxito",
            data: nuevoPost,
          });
        }
      });
    });
  },

  eliminarPost: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      postSchema.findOneAndDelete(
        { _id: id },
        //@ts-ignore
        (error: any) => {
          if (error)
            reject({
              messages: "Hubo un problema con su solicitud",
              success: false,
            });
          else {
            resolve({
              messages: "Entrada eliminado con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  createAds: (root: any, { data }) => {
    const nuevaAds = new adsSchema(data.data);
    return new Promise((resolve, rejects) => {
      nuevaAds.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: "Anuncio creado con exito",
            success: true,
          });
        }
      });
    });
  },

  updateAds: (root: any, { data }) => {
    return new Promise((resolve, rejects) => {
      adsSchema.findOneAndUpdate(
        { _id: data._id },
        data,
        { new: true },
        (error: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              messages: "Anuncio acualizado con exito",
              success: true,
            });
          }
        }
      );
    });
  },

  eliminarAds: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      adsSchema.findOneAndDelete(
        { _id: id },
        //@ts-ignore
        (error: any) => {
          if (error)
            reject({
              messages: "Hubo un problema con su solicitud",
              success: false,
            });
          else {
            resolve({
              messages: "Producto eliminado con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  singleUploadToStoreImagenAws(parent: any, { file }) {
    const buf = Buffer.from(
      file.replace(/^data:image\/\w+;base64,/, ""),
      "base64"
    );
    var data = {
      Key: `${Date.now()}-federacion-burgos`,
      Body: buf,
      ContentEncoding: "base64",
      ContentType: "image/jpeg",
      ACL: "public-read",
    };
    return new Promise((resolve, reject) => {
      //@ts-ignore
      s3BucketStore.upload(data, function (err, data) {
        if (err) {
          reject({
            data: "Imagen muy grandel reduce el tamaño por favor",
          });
        } else {
          resolve({
            data: data,
          });
        }
      });
    });
  },

  createProveedor: (root: any, { input }) => {
    const nuevaproveedorSchema = new proveedorSchema(input);
    return new Promise((resolve, rejects) => {
      nuevaproveedorSchema.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: "Proveedor creado con exito",
            success: true,
          });
        }
      });
    });
  },

  updateProveedor: (root: any, { input }) => {
    return new Promise((resolve, rejects) => {
      proveedorSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              messages: "Anuncio acualizado con exito",
              success: true,
            });
          }
        }
      );
    });
  },

  eliminarProveedor: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      proveedorSchema.findOneAndDelete(
        { _id: id },
        //@ts-ignore
        (error: any) => {
          if (error)
            reject({
              messages: "Hubo un problema con su solicitud",
              success: false,
            });
          else {
            resolve({
              messages: "Producto eliminado con éxito",
              success: true,
            });
          }
        }
      );
    });
  },
};
