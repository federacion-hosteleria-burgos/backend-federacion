import jwt from "jsonwebtoken";

export const crearToken = (user: any, secreto: any, expiresIn: any) => {
  const { _id } = user;

  return jwt.sign({ _id }, secreto, { expiresIn });
};