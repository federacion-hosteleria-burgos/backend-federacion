import userSchema from "../models/user";
import comunicadoSchema from "../models/comunicado";
import { validateEmail } from "./validateEmail";
import postSchema from "../models/post";
import adsSchema from "../models/ads";
import proveedorSchema from "../models/proveedor";
import { Types } from "mongoose";

const { ObjectId } = Types;

export const Query = {
  getUsuario: (root: any, {}, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debes iniciar sesión para continuar",
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: usuarioActual._id }, (err, res) => {
          if (err) {
            rejects({
              message: "Algo salio mal intentalo de nuevo",
              success: false,
              data: {},
            });
          } else {
            resolve({
              message: "Bienbenido a la Federación de Hostelería",
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getComunicados: (root: any, { type }, {}) => {
    return new Promise((resolve, rejects) => {
      comunicadoSchema
        .find({ type: type }, (err, res) => {
          if (err) {
            rejects({
              message: "Algo salio mal intentalo de nuevo",
              success: false,
              data: [],
            });
          } else {
            resolve({
              message: "Todo a salido bien",
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getUserAdmin: async (root: any, { search, page, limit }) => {
    var value = await userSchema.countDocuments();
    return new Promise((resolve, reject) => {
      let condition = {};

      //@ts-ignore
      if (search)
        if (validateEmail(search)) {
          //@ts-ignore
          condition.email = search;
        } else {
          condition = {
            $text: { $search: `"\"${search} \""` },
          };
        }

      userSchema
        .find(condition, (error: any, user: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              count: 0,
              data: null,
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              count: value,
              data: user,
            });
          }
        })
        .sort(search ? null : { $natural: -1 })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },

  getPost: (root: any, {}, {}) => {
    return new Promise((resolve, rejects) => {
      postSchema
        .find((err, res) => {
          if (err) {
            rejects({
              message: "Algo salio mal intentalo de nuevo",
              success: false,
              data: [],
            });
          } else {
            resolve({
              message: "Todo a salido bien",
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getComunicadosID: (root: any, { id }, {}) => {
    if (id) {
      return new Promise((resolve, rejects) => {
        comunicadoSchema.findOne({ _id: ObjectId(id) }, (err, res) => {
          console.log("resd", res, ObjectId(id));
          if (err) {
            rejects({
              message: "Algo salio mal intentalo de nuevo",
              success: false,
              data: null,
            });
          } else {
            resolve({
              message: "Todo a salido bien",
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getPostParam: (root: any, { input, page, limit }, {}) => {
    return new Promise((resolve, rejects) => {
      postSchema
        .find(input, (err, res) => {
          if (err) {
            rejects({
              message: "Algo salio mal intentalo de nuevo",
              success: false,
              data: null,
            });
          } else {
            resolve({
              message: "Todo a salido bien",
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },

  getAds: (root: any, {}) => {
    let matchQuery1 = {};
    return new Promise((resolve, rejects) => {
      adsSchema.aggregate(
        [{ $match: matchQuery1 }, { $sample: { size: 150 } }],
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: "Sisten Error",
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: "Ads success",
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getProveedor: async (root: any, { search, page, limit }) => {
    var value = await proveedorSchema.countDocuments();
    return new Promise((resolve, reject) => {
      let condition = {};
      //@ts-ignore
      if (search)
        condition = {
          //@ts-ignore
          $text: { $search: `"\"${search} \""` },
        };

      proveedorSchema
        .find(condition, (error: any, user: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              count: 0,
              data: null,
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              count: value,
              data: user,
            });
          }
        })
        .sort(search ? null : { $natural: -1 })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },
};
