import { resolvers } from "./Resolvers";
import { typeDefs } from "./Schema";
import { ApolloServer } from "apollo-server-express";
import jwt from "jsonwebtoken";

const GQservers = new ApolloServer({
  typeDefs,
  resolvers,
  playground: true,
  introspection: true,
  context: async ({ req }) => {
    const token = req.headers["authorization"];
    if (token !== null) {
      try {
        const usuarioActual = await jwt.verify(token, process.env.SECRETO);
        req.usuarioActual = usuarioActual;
        return {
          usuarioActual,
        };
      } catch (err) {
        //console.log('err: ', err);
      }
    }
  },
});

export default GQservers;
