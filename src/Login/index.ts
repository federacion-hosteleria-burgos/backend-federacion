import { Router } from "express";
import userSchema, { IUser } from "../models/user";
import bcrypt from "bcryptjs";

const routerLogin = Router();

routerLogin.post("/api/v1/auth/social/mobile", async function (req, res) {
  require("mongoose").model("user").schema.add({
    appleToken: String,
    socialNetworks: String,
    isSocial: Boolean,
  });
  let data = req.body;
  const tokens = data.token;
  let emailExistsApple = await userSchema.findOne({
    appleToken: data.token,
  });

  if (emailExistsApple) {
    const nuevoUsuario = emailExistsApple;
    res.json({ nuevoUsuario, token: tokens });
  } else {
    let emailExists = await userSchema.findOne({
      email: data.email,
    });

    if (emailExists) {
      bcrypt.genSalt(10, (err: any, salt: any) => {
        if (err) console.log(err);
        bcrypt.hash(data.token, salt, (err: any, hash: any) => {
          if (err) console.log(err);
          userSchema.findOneAndUpdate(
            { email: data.email },
            { password: hash },
            //@ts-ignore
            (err: any, user: IUser) => {
              if (err) {
                res.json(err);
              }
              let nuevoUsuario = user;
              res.json({ nuevoUsuario, token: tokens });
            }
          );
        });
      });
    } else {
      const nuevoUsuarios = new userSchema({
        name: data.firstName,
        lastName: data.lastName,
        email: data.email,
        password: data.token,
        isSocial: true,
        city: data.city,
        appleToken: data.token,
        socialNetworks: data.provide,
      });

      nuevoUsuarios.id = nuevoUsuarios._id;

      nuevoUsuarios.save(async (error: any) => {
        if (error) {
          return res.json(error);
        } else {
          /* await stripe.customers.create(
                  {
                    name: data.firstName,
                    email: data.email,
                    description: "Clientes de Wilbby",
                  },
                  function (err: any, customer: any) {
                    userSchema.findOneAndUpdate(
                      { _id: nuevoUsuarios._id },
                      {
                        $set: {
                          StripeID: customer.id,
                        },
                      },
                      (err, customers) => {
                        if (err) {
                          console.log(err);
                        }
                      }
                    );
                  }
                ); */

          const nuevoUsuario = nuevoUsuarios;
          return res.json({ nuevoUsuario, token: tokens });
        }
      });
    }
  }
});

export default routerLogin;
